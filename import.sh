#!/bin/bash
ndir=8							# ndir should match number of params
echo "Making directories and copying cpfile."
for((i=1;i<=$ndir;i++))
do	
    rsync abhins@kohn.physics.udel.edu:projects/active/spin_superfluid/scan1/dir_$i/$1_eq.txt dir_$i/.
    #rsync abhins@kohn.physics.udel.edu:projects/active/spin_superfluid/scan1/dir_$i/def.param dir_$i/.
done
