import numpy as np
def text(inp):
	res = r"$\mathrm{" + inp + "}$"
	res = res.replace(" ", "\ ")
	return res


def extract_tdnegf(data, item='chj_vs_time', ti=0, tf=-1, t = 0, 
				   dt = 0.1, ni=1, nf = -1, N = 10 , n = 10, 
				   en = 1, ps = 1, print_step = 1, verbose=1):
	"""A package to extract tdnegf plotting elements
	ti, tf, t : initial and final time and particular time
	ni, nf, n 
	N : total number of sites
	ps : picoseond boolean
	""" 
	#gettin the index of time
	tn = 1
	if ps: tn = 1000;
	ti = int((ti//dt) // print_step)
	tf = int((tf//dt) // print_step)
	t  = int((t//dt)  // print_step)

	if item == 'spin_vs_site':
		if verbose: print("extracting spin vs site info")
		nx = 3*(N - 1) + 1
		ny = 3*(N - 1) + 2
		nz = 3*(N - 1) + 3
		
		Mxl  = data[3*(ni-1)+1:nx+1:3, t]
		Myl  = data[3*(ni-1)+2:ny+1:3, t]
		Mzl  = data[3*(ni-1)+3:nz+1:3, t]
		
		Mzla = data[3*(ni-1)+3:nz+1:6, t]
		Mzlb = data[3*(ni-1)+6:nz+1:6, t]
		l	 = np.arange(1, n+1, 1)
		ls   = np.arange(1, n+1, 2)
		
		
		return l, Mxl, Myl, Mzl
	
	elif item == 'chj_vs_time':
		if verbose: print("extracting charge current info")
		time = data[0, ti:tf]/tn
		ch   = data[1, ti:tf]/en
		chL  = data[2, ti:tf]/en
		chR  = data[3, ti:tf]/en

		return time, ch, chL, chR
	
	elif item == 'spj_vs_time':
		if verbose: print("extracting spin current info")
		time = data[0, ti:tf]/tn
		sxL   = data[1, ti:tf]/en
		syL   = data[3, ti:tf]/en
		szL   = data[5, ti:tf]/en
		
		sxR   = data[2, ti:tf]/en
		syR   = data[4, ti:tf]/en
		szR   = data[6, ti:tf]/en

		return time, sxL, syL, szL, sxR, syR, szR
	
	elif item == 'spin_vs_time':
		if verbose: print("extracting spin vs time info")
		nx = 3*(n - 1) + 1
		ny = 3*(n - 1) + 2
		nz = 3*(n - 1) + 3
		
		time = data[0,  ti:tf]/tn
		Sx   = data[nx, ti:tf]/en
		Sy   = data[ny, ti:tf]/en
		Sz   = data[nz, ti:tf]/en
		
		return time, Sx, Sy, Sz
	
	elif item == 'spin_vs_site':
		if verbose: print("extracting spin vs time info")
		nx = 3*(n - 1) + 1
		ny = 3*(n - 1) + 2
		nz = 3*(n - 1) + 3
		
		time = data[0,  ti:tf]/tn
		Sx   = data[nx, ti:tf]/en
		Sy   = data[ny, ti:tf]/en
		Sz   = data[nz, ti:tf]/en
		
		return time, Sx, Sy, Sz
	
	elif item == 'fft':
		if verbose: print("doing fft transformation")
		#returning absolute value of fft divided by sample size
		
		#getting back actual time
		ti = ti*dt*print_step
		tf = tf*dt*print_step
		times = np.arange(ti, tf, dt)
		N = times.size
		yif = np.fft.fft(data)
		yf = (2/N)*np.abs(yif[0:N//2])
		xf = np.fft.fftfreq(N, dt)[:N//2]
		return xf, yf
	
	elif item == 'laser':
		if verbose: print("extracting light information")
		#returning absolute value of fft divided by sample size
		
		#getting back actual time
		y = data[0,  ti:tf]
		return y

	
	Mx_t = data[nx,t]
	My_t = data[ny,t]
	Mz_t = data[nz,t]
