#!/bin/bash
ndir=2							# ndir should match number of params
params=(50 60 70 80 90 100 120 140 160 170 180)
No=0
j=`expr 1 + $No`

base="base"
compiler=../base/tdnegf_llg
cpfile=def.param

if [ -d "dir_$j" ]
then
echo "Directories exist! Quiting the script." 
return 0

else
echo "Making directories and copying cpfile."
for((i=1;i<=$ndir;i++))
do	
	j=`expr $i + $No`
	mkdir dir_$j
	cp $base/$cpfile dir_$j/.
done

#setting parameters
echo "Setting parameters."
for((i=1;i<=ndir;i++))
do
	# editing 19th line of parameter file
	j=`expr $i + $No`
	sed -i "19s/.*!/${params[$i-1]}       !/g" dir_$j/$cpfile
	echo "Parameter for dir_$j is ${params[$i-1]}."
done
#runnning the calculation
echo "Starting to run calculations."
{
for((i=1;i<=ndir;i++))
do
	if [ $i != 1 ]
	then
		while kill -0 $BACK_PID; do
	    sleep 1 
		done
		j=`expr $i + $No`
		cd dir_$j
		echo "runnig cal_$j"
		$compiler $cpfile &> out.txt &
		BACK_PID=$!
		cd ..
	else	
		echo "fisrt loop"
		j=`expr $i + $No`
		cd dir_$j
		echo "runnig cal_$j"
		$compiler $cpfile &> out.txt &
		BACK_PID=$!
		echo $BACK_PID
		cd ..
	fi
done
} > out.log &
jobs -l > jobs.log
fi


