#!/usr/bin/env python3
import numpy as np; import sys
import h5py; import os
import warnings

def main():		
	with warnings.catch_warnings():
		warnings.simplefilter("ignore")
	#------------------------------------------------------------
	rm_files= 1
	verbose = 0
	check_2 = 0
	if os.path.exists("seq_eq.txt") and \
	   os.path.exists("sne_eq.txt") and \
	   os.path.exists("rho.txt") and \
	   os.path.exists("scl_eq.txt") and \
	   os.path.exists("spj_eq.txt") and \
	   os.path.exists("chj_eq.txt") and \
	   os.path.exists("bcj_eq.txt"):
		check_2 = 1
	if check_2 == 0:
		print("all files to not present to update data.hdf5")
		sys.exit()

	if rm_files == 1:
		check = input("rm files on, '1' to continue; '0' to exit : ")
		if check != '1':
			rm_files = 0
			if check == '0':
				print("Exiting code")
				sys.exit()
			else:
				print("Continuing without removing .txt files")
					

	with h5py.File('data.hdf5', 'w') as f:
		if os.path.exists("scl_eq.txt"):
			if os.stat('scl_eq.txt').st_size:
				if verbose: print('reading cspins') 	
				data1	  = np.loadtxt('scl_eq.txt', unpack = True)
				shape1 = data1.shape
				dset1 = f.create_dataset("cspins", shape1, dtype='f8',
										compression="gzip")
				dset1[:,:] = data1[:,:]

		if os.path.exists("spj_eq.txt"):
			if os.stat('spj_eq.txt').st_size: 	
				if verbose: print('reading spin currents') 	
				data2	  = np.loadtxt('spj_eq.txt', unpack = True)
				shape2 = data2.shape
				dset2 = f.create_dataset("spj_eq", shape2, dtype='f8',
										compression="gzip")
				dset2[:,:] = data2[:,:]

		if os.path.exists("chj_eq.txt"):
			if os.stat('chj_eq.txt').st_size: 	
				if verbose: print('reading charge currents') 	
				data3	  = np.loadtxt('chj_eq.txt', unpack = True)
				shape3 = data3.shape
				dset3 = f.create_dataset("chj_eq", shape3, dtype='f8',
										compression="gzip")
				dset3[:,:] = data3[:,:]

		if os.path.exists("seq_eq.txt"):
			if os.stat('seq_eq.txt').st_size: 	
				if verbose: print('reading seq') 	
				data4	  = np.loadtxt('seq_eq.txt', unpack = True)
				shape4 = data4.shape
				dset4 = f.create_dataset("seq_eq", shape4, dtype='f8',
										compression="gzip")
				dset4[:,:] = data4[:,:]

		if os.path.exists("sne_eq.txt"):
			if os.stat('sne_eq.txt').st_size: 	
				if verbose: print('reading sneq') 	
				data5	  = np.loadtxt('sne_eq.txt', unpack = True)
				shape5 = data5.shape
				dset5 = f.create_dataset("sne_eq", shape5, dtype='f8',
										compression="gzip")
				dset5[:,:] = data5[:,:]

		if os.path.exists("bcj_eq.txt"):
			if os.stat('bcj_eq.txt').st_size: 	
				if verbose: print('reading bond currents') 	
				data6	  = np.loadtxt('bcj_eq.txt', unpack = True)
				shape6 = data6.shape
				dset6 = f.create_dataset("bcj_eq", shape6, dtype='f8',
										compression="gzip")
				dset6[:,:] = data6[:,:]

		if os.path.exists("rho.txt"):
			if os.stat('rho.txt').st_size: 	
				if verbose: print('reading rho') 	
				data7	  = np.loadtxt('rho.txt', unpack = True)
				shape7 = data7.shape
				dset7 = f.create_dataset("rho", shape7, dtype='f8',
										compression="gzip")
				dset7[:,:] = data7[:,:]
		if os.path.exists("light_Ax.txt"):
			if os.stat('light_Ax.txt').st_size: 	
				if verbose: print('reading light') 	
				data8	  = np.loadtxt('light_Ax.txt',unpack = True)
				shape8 = data8.shape
				dset8 = f.create_dataset("light",shape8,dtype='f8',
										compression="gzip")
				dset8[:,:] = data8[:,:]

	f.close()
	if rm_files:
		print("Removing .txt files")
		if os.path.exists("seq_eq.txt"):
			os.remove("seq_eq.txt")
		if os.path.exists("sne_eq.txt"):
			os.remove("sne_eq.txt")
		if os.path.exists("rho.txt"):
			os.remove("rho.txt")
		if os.path.exists("scl_eq.txt"):
			os.remove("scl_eq.txt")
		if os.path.exists("spj_eq.txt"):
			os.remove("spj_eq.txt")
		if os.path.exists("chj_eq.txt"):
			os.remove("chj_eq.txt")
		if os.path.exists("bcj_eq.txt"):
			os.remove("bcj_eq.txt")
		if os.path.exists("light_Ax.txt"):
			os.remove("light_Ax.txt")
	'''
	#compression_opts=9
	f = h5py.File(sys.argv[1], 'r')
	data1 = f['cspins']
	data2 = f['espins']
	number = '1'	# figure number
	'''
if __name__ == main():
	main()
