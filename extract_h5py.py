import h5py as h5py
import numpy as np

def extract_band(data, verbose, index):
    '''
    Extract bandstructure details from ATK hdf5 file
    '''
    
    f = h5py.File(data, 'r')
    band = f['Bandstructure_'+index]

    kpath = band['BaseBandstructure']['path']['data'][()]
    plot_x = np.linspace(0, 1, num = len(kpath))
    x = plot_x.reshape((1,len(kpath)))

    bandev = band['eigenvalues']['array']['data'][()][0,:,:].T
    l1,l2,l3 = band['eigenvalues']['array']['data'][()].shape

    band_st = np.concatenate((x, bandev))
    kpts = band['BaseBandstructure']['path']['data'][()].T
    if l1 == 2:
        bandev2 = band['eigenvalues']['array']['data'][()][1,:,:].T
        band_st = np.concatenate((x, bandev))
        band_st = np.concatenate((band_st, bandev2))
        if verbose:
            print('energy unit:', band['eigenvalues']['unit']['data'][()])
            print('atk shape: ',band_st.shape)
        return band_st, kpts, l1
    if verbose:
        print('energy unit:', band['eigenvalues']['unit']['data'][()])
        print('atk shape: ',band_st.shape)

    return band_st, kpts, l1
    
