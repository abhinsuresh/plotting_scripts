#!/bin/bash
ndir=11							# ndir should match number of params
params=(50 60 70 80 90 100 120 140 160 170 180)
No=13
j=`expr 1 + $No`
if [ -d "dir_$j" ]
then 
return 0
else
for((i=1;i<=$ndir;i++))
do	
	j=`expr $i + $No`
	echo $j
	[ -d "dir_$j" ] && echo "Directory exist"
done
fi
