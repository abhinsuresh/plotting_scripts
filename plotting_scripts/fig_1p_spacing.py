#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import sys
from tex import text as text
from rcparams import rc_update
import matplotlib.gridspec as gridspec
def main():     
    from matplotlib import rcParams
    rc_update(rcParams, fs = 9, tex = 1, minor = 1, lw = 0.4)
    #------------------------------------------------------------
    #------------------------------------------------------------
    data     = np.loadtxt(sys.argv[1], unpack = True)
    spacing = data[0,:]
    energy  = data[1,:]    
    xlabel = "Spacing (\AA)"
    ylabel = "Energy (eV)" 
    units  = 0  # 1: atomic, 0: ev_angstrom
    if units:
        spacing = spacing#*0.529177
        energy  = energy*27.2114
        xlabel = "k points"
    plot_x = spacing
    plot_y = energy #- energy[-1]
    number = 'spacing'    # figure number
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=0.87, bottom=0.23, 
                           left=0.3, right=0.7, 
                           wspace=0, hspace=0)
    ax1 = plt.subplot(gs[0,0]) 
    #------------------------------------------------------------
    #------------------------------------------------------------    
    ax1.set_xlabel(text(xlabel), labelpad = 1)
    ax1.set_ylabel(text(ylabel),  labelpad=1)
    ax1.set_xlim((0.55,0.1))
    #ax1.set_ylim((-121.4,-121.6))
    #ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
    ax1.plot(plot_x, plot_y, '-o', ms = 0.7)
    plt.savefig('fig' + number + '.pdf')
if __name__ == main():
    main()

