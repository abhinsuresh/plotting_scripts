#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
from tex import extract_tdnegf as et
import matplotlib.gridspec as gridspec

def main():		
	from matplotlib import rcParams
	rc_update(rcParams, fs = 8, tex = 1, minor = 1, lw = 0.5)
	#------------------------------------------------------------
	data1	  = np.loadtxt(sys.argv[1], unpack = True)
	data2	  = np.loadtxt(sys.argv[2], unpack = True)
	number = 'fftl'	# figure number
	#------------------------------------------------------------
	item = 'spj_vs_time'#'spin_vs_site''spin_vs_time'
	#plt.rcParams["font.family"] = "Times New Roman"
	#n	= 4; t = -1;
	#plot_x, Mxl, Myl, Mzl = get_info(data1, item, t = -t, n = n)
	#time, ch, chL, chR = et(data1, item, ti = 970, tf= 1150, 
	#					    dt = 0.01, print_step = 1)
	time,sxL,syL,szL,sxR,syR,szR = et(data1, item, ti=900, tf=1200,
							dt = 0.01, print_step=1)
	y = et(data2, 'laser', ti=900, tf=1200, dt=0.01)
	lxf, lyf = et(y,   'fft',  ti=900, tf=1200, dt=0.01)
	xxf, xyf = et(sxR, 'fft',  ti=900, tf=1200, dt=0.01)
	yxf, yyf = et(syR, 'fft',  ti=900, tf=1200, dt=0.01)
	zxf, zyf = et(szR, 'fft',  ti=900, tf=1200, dt=0.01)
	
	#------------------------------------------------------------
	gs = gridspec.GridSpec(1, 1, 
						   top=0.87, bottom=0.2, 
						   left=0.3, right=0.7, 
						   wspace=0, hspace=0)
	ax1 = plt.subplot(gs[0,0])
	#------------------------------------------------------------
	ax1.set_xlim((0,700))
	ax1.set_xlabel(r'$\mathrm{\omega (1/ps)}$', labelpad = 1)
	ax1.set_ylabel(r'$\mathrm{FFT\ Power\ (a.u.)}$',
					labelpad=1)
	#ax1.text(0.05,0.98,r'$\mathrm{(a)}$', transform=ax1.transAxes,
	#		  fontsize=9,va ='top')    
	ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
	
	#ax1.plot(time, Sx, 'C0')
	#ax1.plot(time, Sy, 'C2')
	#ax1.plot(time, Sz, 'C3')
	
	#ax1.plot(time, Mx, time, Mx, time, My)
	#ax1.plot(plot_x, Mzl, 'k')
	ax1.plot(lxf*1000, lyf, 'k' )
	ax1.plot(xxf*1000, xyf, 'C0')
	ax1.plot(yxf*1000, yyf, 'C2')
	ax1.plot(zxf*1000, zyf, 'C3')
	#ax1.plot(plot_x, Mzlb, 'C3', ls = (0, (5, 5)))
	
	plt.savefig('fig' + number + '.pdf')
	
if __name__ == main():
	main()
