#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib import rcParams
import matplotlib.ticker as mticker
import matplotlib
#from matplotlib.patches import Ellipse
import matplotlib.gridspec as gridspec
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
#rcParams.update({'figure.autolayout': True})
#rcParams.update({'lines.linewidth': 1.0})
rcParams.update({'text.usetex': True})
#rcParams.update({'axes.labelpad': 4.0})
rcParams.update({'xtick.top': False})
rcParams.update({'xtick.bottom': True})
rcParams.update({'xtick.labeltop': False})
rcParams.update({'xtick.labelbottom': True})
rcParams.update({'xtick.direction': 'in'})
rcParams.update({'ytick.direction': 'in'})

rcParams.update({'legend.fontsize': 7})
rcParams.update({'ytick.labelsize': 7})
rcParams.update({'xtick.labelsize': 7})
rcParams.update({'axes.labelsize': 7})
rcParams.update({'xtick.minor.visible':True})
rcParams.update({'xtick.major.pad': 3})
rcParams.update({'lines.linewidth': 0.6})
rcParams.update({'axes.linewidth': 0.6})
rcParams.update({'xtick.major.size': 1})
rcParams.update({'ytick.major.size': 1})
rcParams.update({'ytick.minor.size': 0.7})
rcParams.update({'xtick.minor.size': 0.7})
#rcParams.update({''})
#rcParams.update({'xtick.major.size': 10})
rcParams.update({'ytick.major.pad': 4})
#rcParams.update({'ytick.labelright': False})
rcParams.update({'figure.figsize': [7.08,2.7]})
rcParams.update({'figure.dpi': 300})



def main():     
    #------------------------------------------------------------
    #------------------------------------------------------------
    # import data
    infile1  = sys.argv[1]
    infile2  = sys.argv[2]
    infile3  = sys.argv[3]
    infile4  = sys.argv[4]
    infile5  = sys.argv[5]
    data   = np.loadtxt(infile1, unpack = True)
    data2  = np.loadtxt(infile2, unpack = True)
    data3  = np.loadtxt(infile3, unpack = True)
    data4  = np.loadtxt(infile4, unpack = True)
    data5  = np.loadtxt(infile5, unpack = True)

    # 1st index is column
    t1 = 950
    t2 = 7000
    
    j1 = 930
    j2 = 1650
    
    k1 = 800
    k2 = 2000
    
    icl = 0.5
    isl = 0.5

    #lu_y = r'$\mathrm{Pulse\ height\ (meV)}$'
    lu_x = r'$\mathrm{Time\ (ps)}$'
    lu_lpx = 2
    lu_lpy = 2

    N       = 28
    site_no = 25
    xn      = 1*(N-1) + site_no
    yn      = 2*(N-1) + site_no
    zn      = 3*(N-1) + site_no    
    
    site_no = 5
    xe      = 1*(N-1) + site_no
    ye      = 2*(N-1) + site_no
    ze      = 3*(N-1) + site_no    
    
    N       = 8
    site_no = 5
    xm      = 1*(N-1) + site_no
    ym      = 2*(N-1) + site_no
    zm      = 3*(N-1) + site_no    
     
    name = '4'    # figure number

    #------------------------------------------------------------
    #------------------------------------------------------------
    fig = plt.figure()
    gs = gridspec.GridSpec(2, 4, 
                           top=0.95, bottom=0.098, 
                           left=0.063, right=0.99, 
                           wspace=0.37, hspace=0.27)
    ax1 = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0])
    ax4 = plt.subplot(gs[1,1])
    ax5 = plt.subplot(gs[0,2])
    ax6 = plt.subplot(gs[0,3])
    ax7 = plt.subplot(gs[1,2])
    ax8 = plt.subplot(gs[1,3])
    
    #------------------------------------------------------------
    #------------------------------------------------------------
    ax1.set_ylabel(r'$I_{\rm AFI\rightarrow FM}^{S_x}/V_H\  (e^2/h)$',
                    labelpad=0)
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
    #ax1.xaxis.set_major_formatter(plt.NullFormatter())
    ax1.xaxis.set_minor_locator(AutoMinorLocator(2))
    ax2.xaxis.set_minor_locator(AutoMinorLocator(2))
    ax3.xaxis.set_minor_locator(AutoMinorLocator(2))
    ax4.xaxis.set_minor_locator(AutoMinorLocator(2))
    ax5.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax6.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax7.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax8.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax1.set_ylim(-0.007, 0.010)
    ax1.text(-0.26,1.13,r'($\mathbf{a}$)',transform=ax1.transAxes,
             fontsize=8,va ='top')

    time = data[0, t1:t2]
    Sx   = data[xn, t1:t2]
    time = time/1000
    Sx   = Sx/0.55
    ax1.plot(time,Sx,'C0', label=r'$J_{\rm AFI-FM}\neq 0$')
    
    time = data2[0, t1:t2]
    Sx   = data2[xn, t1:t2]
    time = time/1000
    Sx   = Sx/0.55
    ax1.plot(time,Sx,'C0', label=r'$J_{\rm AFI-FM}=0$', ls = ':')
    ax1.legend(loc='upper right',ncol=1,frameon=False, 
               handlelength = 1,
               columnspacing = 0.2, handletextpad=0.05)
    #------------------------------------------------------------
    #------------------------------------------------------------
   
    ax2.set_ylabel(r'$I_{\rm AFI\rightarrow FM}^{S_y}/V_H\  (e^2/h)$', 
                    labelpad=0)
    ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-2,-2))
    ax2.set_ylim(-0.008, 0.0075)

    ax2.text(-0.26,1.13,r'($\mathbf{b}$)',transform=ax2.transAxes,
             fontsize=8,va ='top')
    time = data[0,  t1:t2]
    Sy   = data[yn, t1:t2]
    time = time/1000
    Sy   = Sy/0.55
    ax2.plot(time,Sy,'C2', label=r'$J_{\rm AFI-FM}\neq 0$')
    time = data2[0, t1:t2]
    Sy   = data2[yn, t1:t2]
    time = time/1000
    Sy   = Sy/0.55
    ax2.plot(time,Sy,'C2', label=r'$J_{\rm AFI-FM}=0$',
             ls = ':')
    ax2.legend(loc='upper right',ncol=1,frameon=False,
               columnspacing = 0.2, handletextpad=0.05,
               handlelength = 1)
    #------------------------------------------------------------
    #------------------------------------------------------------
    ax3.set_ylabel(r'$I_{\rm AFI\rightarrow FM}^{S_z}/V_H\  (e^2/h)$', 
                    labelpad=6)
    ax3.set_xlabel(lu_x, labelpad=lu_lpx)
    ax3.ticklabel_format(axis ='y', style='sci', scilimits=(-2,-2))
    ax3.text(-0.26,1.13,r'($\mathbf{c}$)',transform=ax3.transAxes,
             fontsize=8,va ='top')
    ax3.set_ylim(-0.004, 0.008)
    ax3.set_yticks(np.arange(-0.00,0.006,0.005))
    time = data[0, t1:t2]
    Sz   = data[zn, t1:t2]
    time = time/1000
    Sz   = Sz/0.55
    ax3.plot(time, Sz,'C3', label=r'$J_{\rm AFI-FM}\neq 0$')
    time = data2[0, t1:t2]
    Sz   = data2[zn, t1:t2]
    time = time/1000
    Sz   = Sz/0.55
    ax3.plot(time, Sz,'C3', label=r'$J_{\rm AFI-FM}=0$',
             ls = ':') 
    ax3.legend(loc='upper right',ncol=1,frameon=False,
               columnspacing = 0.2, handletextpad=0.05,
               handlelength = 1)
    #------------------------------------------------------------
    #------------------------------------------------------------
    
    ax4.set_ylabel(r'$I_{\rm NM\rightarrow AFI}^{S_{\alpha}}/V_H\ (e^2/h)$', 
                   labelpad=6.3)
    ax4.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad=lu_lpx)
    ax4.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
    ax4.text(-0.26,1.13,r'($\mathbf{d}$)',transform=ax4.transAxes,
             fontsize=8,va ='top')
    time = data[0,  k1:k2]
    Sz   = data[ze, k1:k2]
    Sy   = data[ye, k1:k2]
    Sx   = data[xe, k1:k2]
    time = time/1000
    Sz   = Sz/0.55
    Sy   = Sy/0.55
    Sx   = Sx/0.55
    ax4.plot(time, Sx,'C0', label=r'$\mathrm{I^{S_x}}$')
    ax4.plot(time, Sy,'C2', label=r'$\mathrm{I^{S_y}}$')
    ax4.plot(time, Sz,'C3', label=r'$\mathrm{I^{S_z}}$')
    ax4.text(0.6,0.98,r'$\alpha =$',transform=ax4.transAxes,
             fontsize=7,va ='top', color='k')
    ax4.text(0.8,0.98,r'$x$',transform=ax4.transAxes,
             fontsize=7,va ='top', color='C0')
    ax4.text(0.87,0.98,r'$y$',transform=ax4.transAxes,
             fontsize=7,va ='top', color='C2')
    ax4.text(0.94,0.98,r'$z$',transform=ax4.transAxes,
             fontsize=7,va ='top', color='C3')
    #ax4.legend(loc='upper right',ncol=3,frameon=False,
    #           columnspacing = 0.2, handletextpad=0.05,
    #           handlelength = 1)

    #------------------------------------------------------------
    #------------------------------------------------------------
 
    ax5.set_ylabel(r'$I_{\rm NM\rightarrow FM}^{S_x}/V_H\  (e^2/h)$', 
                    labelpad=9)
    ax5.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
    ax5.text(-0.26,1.13,r'($\mathbf{e}$)',transform=ax5.transAxes,
             fontsize=8,va ='top')
    ax5.set_xticks(np.arange(1,1.6,0.5))
  
    time = data4[0,  j1:j2]
    Sx   = data4[xm, j1:j2]
    time = time/1000
    Sx   = Sx/0.25
    ax5.plot(time,Sx,'C0', label=r'$J_{\rm AFI-FM}\neq 0$')
    
    #------------------------------------------------------------
    #------------------------------------------------------------
 
    ax6.set_ylabel(r'$I_{\rm NM\rightarrow FM}^{S_y}/V_H\  (e^2/h)$', 
                    labelpad=1)
    ax6.ticklabel_format(axis ='y', style='sci', scilimits=(-1,-1))
    ax6.text(-0.26,1.13,r'($\mathbf{f}$)',transform=ax6.transAxes,
             fontsize=8,va ='top')
    ax6.set_xticks(np.arange(1,1.6,0.5))
    
    time = data4[0,  j1:j2]
    Sy   = data4[ym, j1:j2]
    time = time/1000
    Sy   = Sy/0.25
    ax6.plot(time,Sy,'C2', label=r'$J_{\rm AFI-FM}\neq 0$')
    #------------------------------------------------------------
    #------------------------------------------------------------
 
    ax7.set_ylabel(r'$I_{\rm NM\rightarrow FM}^{S_z}/V_H\  (e^2/h)$', 
                    labelpad=2)
    ax7.set_xlabel(lu_x, labelpad=lu_lpx)
    ax7.ticklabel_format(axis ='y', style='sci', scilimits=(-1,-1))
    ax7.set_yticks(np.arange(0,0.051,0.05))
    ax7.set_xticks(np.arange(1,1.6,0.5))
    ax7.text(-0.26,1.13,r'($\mathbf{g}$)',transform=ax7.transAxes,
             fontsize=8,va ='top')
    
    time = data4[0,  j1:j2]
    Sz   = data4[zm, j1:j2]
    time = time/1000
    Sz   = Sz/0.25
    ax7.plot(time,Sz,'C3', label=r'$J_{\rm AFI-FM}\neq 0$')
    #------------------------------------------------------------
    #------------------------------------------------------------
 
    ax8.set_ylabel(r'$I_L/V_H\ (e^2/h)$',labelpad=6)
    ax8.set_xlabel(lu_x, labelpad=lu_lpx)
    ax8.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
    ax8.text(-0.26,1.13,r'($\mathbf{h}$)',transform=ax8.transAxes,
             fontsize=8,va ='top')
    ax8.set_xticks(np.arange(1,1.6,0.5))
    
    time = data5[0, j1:j2]
    I_L  = data5[2, j1:j2]/0.25
    I_R  = data5[3, j1:j2]/0.25
    time = time/1000
    ax8.plot(time,I_L,'k', label=r'$\mathrm{I_L}$')
    
    plt.savefig('fig'+ name +'.pdf')   


if __name__ == main():
    main()
