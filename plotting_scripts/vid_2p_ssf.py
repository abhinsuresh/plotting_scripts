#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
import matplotlib.gridspec as gridspec
from tex import text as text
from rcparams import rc_update
import os; import shutil
from tex import extract_tdnegf as et
import h5py
def main():
	from matplotlib import rcParams
	rc_update(rcParams, fs = 9, tex = 1, minor = 0)
	if os.path.exists('plot'):
		shutil.rmtree('plot')
	os.mkdir('plot')	
	f = h5py.File(sys.argv[1], 'r')
	data = f['cspins']
	#---------------------------------------------------
	# to edit
	N = 10; ni = 1
	print_step	= 1    #(1/print_step)
	dt = 0.01
	ti = 2000;  ti_i = int((ti//dt)  //print_step)
	tf = 2500; tf_i = int((tf//dt)  //print_step)
	t_step = 200
	times = data[0,ti_i:tf_i:t_step]
	#print(times[10])
	#---------------------------------------------------
	nn = 3*N + 1
	x = np.arange(1,2*(N-ni+1),2)
	y = np.zeros(N-ni+1) 
	count = 1
	for i, time in enumerate(times):
		#j = plot_step*i*print_step + t0
		#t_step = 
		#print(j//print_step)
		l, Mxl, Myl, Mzl = et(data,'spin_vs_site', t=time, N=N,
					verbose=0, dt = 0.01)

		fig = plt.figure() 
		gs = gridspec.GridSpec(2,1,top=0.98,bottom=0.05,left=0.05,
							   right=0.98, wspace=0.4, hspace=0.1)
		ax1 = plt.subplot(gs[0,0])
		ax2 = plt.subplot(gs[1,0])

		ax1.set_ylabel(r'$\mathrm{M_{x,y}}$',labelpad=0)
		ax2.set_ylabel(r'$\mathrm{M_{z}}$',labelpad=0)
		ax1.set_xlim(-1, 2*(N-ni+1)+1)
		ax2.set_ylim((-1.5, 1.5))
		ax1.set_ylim((-1.2, 1.2))
		ax1.set_xticks([])
		ax1.set_yticks([])
		ax2.set_xticks([])
		ax2.set_yticks([])

		ax1.quiver(x, y, Mxl, Myl, 
				   alpha=1, units = 'dots', scale_units='x',
				   width = 3, scale = 1) 
		
		ax2.quiver(x, y, Mxl*0, Mzl, 
				   alpha=1, units = 'dots', scale_units='y',
				   width = 3, scale = 1) 
	
		ax1.text(0.3,0.98,r'$\mathrm{%s \ (fs)}$'%(str(time)),color='C0',
				 transform=ax1.transAxes, va ='top', fontsize=9) 
		plt.savefig( 'plot/M_'+ str(count) + '.jpeg')
		plt.close(fig)
		count +=1
		#if j >= len(times)-plot_step: return 0
if __name__ == main():
	main()
