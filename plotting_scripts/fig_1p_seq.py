#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
from tex import extract_tdnegf as get_info
import matplotlib.gridspec as gridspec

def main():		
	from matplotlib import rcParams
	rc_update(rcParams, fs = 9, tex = 0, minor = 1)
	#------------------------------------------------------------
	data1	  = np.loadtxt(sys.argv[1], unpack = True)
	number = '1'	# figure number
	#------------------------------------------------------------
	n	= 4; t = -1;
	item = 'spin_vs_site'
	plot_x, Mxl, Myl, Mzl = get_info(data1, item, t = -t, n = n)
	#------------------------------------------------------------
	gs = gridspec.GridSpec(1, 1, 
						   top=0.87, bottom=0.2, 
						   left=0.3, right=0.7, 
						   wspace=0, hspace=0)
	ax1 = plt.subplot(gs[0,0])
	#------------------------------------------------------------
	#ax1.set_ylim((-1.2,1.4))
	ax1.set_xlabel(r'$\mathrm{Sites}$', labelpad = 1)
	ax1.set_ylabel(r'$\mathrm{M_z}$',
					labelpad=1)
	#ax1.text(0.05,0.98,r'$\mathrm{(a)}$', transform=ax1.transAxes,
	#		  fontsize=9,va ='top')    
	ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
	#ax1.plot(time, Mx, time, Mx, time, My)
	ax1.plot(plot_x, Mzl, 'k')
	#ax1.plot(plot_x, Mzlb, 'C3', ls = (0, (5, 5)))
	
	plt.savefig('fig' + number + '.pdf')

if __name__ == main():
	main()
