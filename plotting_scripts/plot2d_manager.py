#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
import matplotlib.gridspec as gridspec
import h5py
from matplotlib.ticker import FormatStrFormatter
values = dict();values['x_plot']='x_plot';values['y_plot']='y_plot'
values['ylabel']=r'$\mathrm{f(x)}$';values['xlabel']=r'$\mathrm{x}$'


def plotfig(data, fig_name, values=values, twin_axis=False,
            text=False,  **kwargs):		
    from matplotlib import rcParams
    rc_update(rcParams, **kwargs)
    number = fig_name
    KB = 8.6173324e-5
    #------------------------------------------------------------
    f = data
    plot_x = f[values['x_plot']]
    plot_y = f[values['y_plot']]
    xlabel = values['xlabel']
    ylabel = values['ylabel']
    #------------------------------------------------------------
    #------------------------------------------------------------
    gs = gridspec.GridSpec(1, 1, 
                           top=0.87, bottom=0.2, 
                           left=0.3, right=0.7, 
                           wspace=0, hspace=0)
    ax1 = plt.subplot(gs[0,0])
    #------------------------------------------------------------
    #ax1.set_ylim((-1.2,1.4))
    ax1.set_xlabel(xlabel, labelpad=1)
    ax1.set_ylabel(ylabel, labelpad=3)
    #ax1.set_ylabel(r'$\mathrm{I_L/\hbar \omega\ (e^2/h)}$', labelpad=1)
    #ax1.text(0.05,0.98,r'$\mathrm{(a)}$', transform=ax1.transAxes,
    #		  fontsize=9,va ='top')    
    ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-2,2))
    #ax1.yaxis.set_major_locator(plt.MaxNLocator(3))

    
    if twin_axis:
        ax2 = ax1.twinx()
        ax2.set_ylim(-1e-6,1e-6)
        ax2.set_ylabel(r'$\mathrm{{\langle \hat{S^{\alpha}} \rangle}_2}$',
                rotation=270,labelpad=10) 
    else: ax2 = ax1 
    if text:
        ax1.text(0.5,0.18,r'$\mathrm{\alpha=}$',color='k', transform=ax1.transAxes,va ='top', fontsize = 8)
        ax1.text(0.7,0.18,r'$\mathrm{x}$',color='C3', transform=ax1.transAxes,va ='top', fontsize=8)
        ax1.text(0.8,0.18,r'$\mathrm{y}$',color='C2', transform=ax1.transAxes,va ='top', fontsize=8)
    #ax1.text(0.54,0.10,r'$\mathrm{fs\ Laser\ Pulse}$',color='k', transform=ax1.transAxes,va ='top', fontsize=6)
    ax1.text(0.3,0.15,r'$\mathrm{g\mu_{\small B}B_1=10^{-3}\ eV}$',color='k', transform=ax1.transAxes,va ='top', fontsize=6)
    ax1.text(0.6,0.90,r'$\mathrm{i=1,3,6,8}$',color='k', transform=ax1.transAxes,va ='top', fontsize=6)
    ax1.text(0.6,0.80,r'$\mathrm{t=-1\ eV}$',color='k', transform=ax1.transAxes,va ='top', fontsize=6)
    ax1.text(0.6,0.70,r'$\mathrm{U=8\ eV}$',color='k', transform=ax1.transAxes,va ='top', fontsize=6)
    #ax1.plot(plot_x[15:], np.abs(data1[15:]), 'k')
    #ax1.scatter(plot_x, np.abs(data1), s=1)
    #ax1.plot(plot_x, data2, 'C3')
    #ax1.plot(plot_x, data1, 'k', ls=':')
    
    if len(plot_y.shape) == 1 or 1 in plot_y.shape:
        ax1.plot(plot_x, plot_y, 'k')
    else:
        ax1.plot(plot_x, plot_y[:,0], 'C3')
        for ind in range(plot_y.shape[1]):
            if ind == 1:
                ax2.plot(plot_x, plot_y[:,ind], 'k', ls='-')
                #ax1.plot(plot_x, plot_y[:,ind], 'C1')
            if ind == 2:
                ax2.plot(plot_x, plot_y[:,ind], 'C2', ls='--')
            if ind == 3:
                ax1.plot(plot_x, plot_y[:,ind], 'C1', ls=':')
    #ax1.set_ylim((-0.2,0.6))
    ax1.set_xlim((0,300))
    #ax1.set_ylim((0,1))
    #ax1.set_yscale('log')
    plt.savefig('results/fig_' + number + '.pdf')

