#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
from tex import extract_tdnegf as et
import matplotlib.gridspec as gridspec
import h5py

def main():		
	from matplotlib import rcParams
	rc_update(rcParams, fs = 8, tex = 1, minor = 1, lw = 0.5)
	#------------------------------------------------------------
	#data1	  = np.loadtxt(sys.argv[1], unpack = True)
	f = h5py.File(sys.argv[1], 'r')
	data1 = f['spj_eq']
	number = 'sp'	# figure number
	#------------------------------------------------------------
	item = 'spj_vs_time'#'spin_vs_site''spin_vs_time'
	#n	= 4; t = -1;
	#plot_x, Mxl, Myl, Mzl = get_info(data1, item, t = -t, n = n)
	#time, ch, chL, chR = et(data1, item, ti = 500, tf= 900, 
	#					    dt = 0.01, print_step = 10)
	#time, Sx, Sy, Sz = et(data1, item, ti=300, tf=1000, dt = 0.01,
	#					  n = 2)
	time,sxL,syL,szL,sxR,syR,szR = et(data1, item, ti=3000, tf=5700,
							dt = 0.1, print_step=10)
	
	#------------------------------------------------------------
	gs = gridspec.GridSpec(1, 2, 
						   top=0.87, bottom=0.22, 
						   left=0.11, right=0.96, 
						   wspace=0.4, hspace=0)
	ax1 = plt.subplot(gs[0,0])
	ax2 = plt.subplot(gs[0,1])
	#------------------------------------------------------------
	#ax1.set_ylim((-1.2,1.4))
	ax1.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad = 1)
	ax1.set_ylabel(r'$\mathrm{I_L^{S_{\alpha}}/\hbar\omega\ (e^2/h)}$',
					labelpad = 1)
	#ax1.text(0.05,0.98,r'$\mathrm{(a)}$', transform=ax1.transAxes,
	#		  fontsize=9,va ='top')    
	ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
	
	#------------------------------------------------------------
	ax2.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad = 1)
	ax2.set_ylabel(r'$\mathrm{I_R^{S_{\alpha}}/\hbar\omega\ (e^2/h)}$',
					labelpad = 1)
	#ax2.text(0.05,0.98,r'$\mathrm{(b)}$', transform=ax2.transAxes,
	#		  fontsize=9,va ='top')    
	ax2.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
	
	if item == 'spj_vs_time':
		ax1.plot(time, sxL, 'C0')
		ax1.plot(time, syL, 'C2')
		ax1.plot(time, szL, 'C3')
		ax2.plot(time, sxR, 'C0')
		ax2.plot(time, syR, 'C2')
		ax2.plot(time, szR, 'C3')
	
	plt.savefig('fig' + number + '.pdf')

if __name__ == main():
	main()
