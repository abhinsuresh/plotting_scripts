import numpy as np
import h5py
import sys
import matplotlib.pyplot as plt

sys.path.append('../../plot_manager/')
from plot2d import plotfig
from rcparams import rc_update


f = h5py.File(sys.argv[1], 'r')
name = '.2'
data = 2*f['spin1'][:]
#data = f['spin1'][:]

zvals = data.reshape(4,2, order='F')
z_up = np.amax(np.abs(zvals))
print('max val: ', z_up)
#z_up = 1e-10

import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib import rcParams
rc_update(rcParams, fs=8)

for t_ind, ti in enumerate([0]):
    gs = gridspec.GridSpec(1, 1, 
                           top=0.87, bottom=0.2, 
                           left=0.3, right=0.7, 
                           wspace=0, hspace=0)
    ax1 = plt.subplot(gs[0,0])
    ax1.set_xticklabels([])
    ax1.set_yticklabels([])
    #fig = plt.figure()
    zvals = data.reshape(4,2, order='F')
    ax1.text(1,1.2,r'$\mathrm{\langle\hat{S}^z_i\rangle}$',color='k',
            transform=ax1.transAxes, va ='top', fontsize=8)
    c = ax1.pcolor(zvals, edgecolors='k', linewidths=1, 
                    cmap='jet', vmin=-z_up, vmax=z_up)
    plt.colorbar(c, ax= ax1)
    plt.savefig('results/cb_'+sys.argv[1][8:-5]+name+'.pdf')
    #plt.close(fig)
