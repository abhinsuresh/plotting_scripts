#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from rcparams import rc_update
from tex import extract_tdnegf as et
import matplotlib.gridspec as gridspec
import h5py
def main():		
	from matplotlib import rcParams
	rc_update(rcParams, fs = 8, tex = 1, minor = 1, lw = 0.5)
	#------------------------------------------------------------
	#data1	  = np.loadtxt(sys.argv[1], unpack = True)
	f = h5py.File(sys.argv[1], 'r')
	#data1 = f['chj_eq'];item ='chj_vs_time';number = 'chj'	
	data1 = f['cspins'];item ='spin_vs_time';number = 'scl'	
	#------------------------------------------------------------
	if item =='chj_vs_time':
		number = 'chj'	
		time, ch, chL, chR = et(data1, item, ti = 900, tf= 10000, 
								dt = 0.01, print_step=10)
	elif item =='spin_vs_time':
		number = 'scl'	
		time, Sx, Sy, Sz = et(data1, item, ti=0, tf=100, 
							dt = 0.01, n = 1, print_step = 10)
	#------------------------------------------------------------
	gs = gridspec.GridSpec(1, 1, 
						   top=0.87, bottom=0.2, 
						   left=0.3, right=0.7, 
						   wspace=0, hspace=0)
	ax1 = plt.subplot(gs[0,0])
	#------------------------------------------------------------
	#ax1.set_ylim((-1.2,1.4))
	ax1.set_xlabel(r'$\mathrm{Time\ (ps)}$', labelpad = 1)
	ax1.set_ylabel(r'$\mathrm{M_{\alpha}}$', labelpad=1)
	#ax1.set_ylabel(r'$\mathrm{I_L/\hbar \omega\ (e^2/h)}$', labelpad=1)
	#ax1.text(0.05,0.98,r'$\mathrm{(a)}$', transform=ax1.transAxes,
	#		  fontsize=9,va ='top')    
	ax1.ticklabel_format(axis ='y', style='sci', scilimits=(-1,1))
	
	if item =='chj_vs_time':
		ax1.plot(time, chL, 'b')
		ax1.plot(time, chR, 'k')
	
	elif item =='spin_vs_time':
		ax1.plot(time, Sx, 'C0')
		ax1.plot(time, Sy, 'C2')
		ax1.plot(time, Sz, 'C3')
	
	#ax1.plot(time, Mx, time, Mx, time, My)
	#ax1.plot(plot_x, Mzl, 'k')
	#ax1.plot(plot_x, Mzlb, 'C3', ls = (0, (5, 5)))
	
	plt.savefig('fig' + number + '.pdf')

if __name__ == main():
	main()
