#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
import matplotlib.gridspec as gridspec
from tex import text as text
from rcparams import rc_update
import os; import shutil
def main():
    from matplotlib import rcParams
    rc_update(rcParams, fs = 9, tex = 1, minor = 0)
    if os.path.exists('plot'):
        shutil.rmtree('plot')
    os.mkdir('plot')
    in_file1 = sys.argv[1]
    data = np.loadtxt(in_file1, unpack = True)
    times = data[0,:]
    #---------------------------------------------------
    # to edit
    plot_step   = 1
    i_start     = 1
    n           = 4
    print_step  = 1    #(1/print_step)
    t0 = 11000
    #---------------------------------------------------
    nn = 3*n + 1
    x = np.arange(1,2*(n-i_start+1),2)
    y = np.zeros(n-i_start+1) 
    count = 1
    for i, time in enumerate(times):
        j = plot_step*i*print_step + t0
        print(j//print_step)
        x_direct = data[3*(i_start-1)+1:nn:3, j]
        y_direct = data[3*(i_start-1)+2:nn:3, j]
        z        = data[3*(i_start-1)+3:nn:3, j] 
        
        x_1 = data[3*(i_start-1)+1:nn:3, j]
        y_1 = data[3*(i_start-1)+2:nn:3, j]
        z_1 = data[3*(i_start-1)+3:nn:3, j] 
        z_l_x = np.arange(i_start,n+1)
        
        fig = plt.figure() 
        gs = gridspec.GridSpec(1, 1, top=0.98, bottom=0.05, left=0.05,
                               right=0.98, wspace=0.4, hspace=0.1)
        ax1 = plt.subplot(gs[0,0])

        ax1.set_ylabel(r'$\mathrm{M_{\alpha}}$',labelpad=0)
        ax1.set_xlim(-1, 2*(n-i_start+1)+1)
        ax1.set_xticks([])
        ax1.set_yticks([])
        ax1.quiver(x, y, x_direct, y_direct, 
                   alpha=1, units = 'dots', scale_units='x',
                   width = 3, scale = 1) 
    
        ax1.text(0.3,0.98,r'$\mathrm{%s \ (fs)}$'%(str(data[0,j])),color='C0',
                 transform=ax1.transAxes, va ='top', fontsize=9) 
        plt.savefig( 'plot/M_'+ str(count) + '.jpeg')
        plt.close(fig)
        count +=1
        if j >= len(times)-plot_step: return 0

if __name__ == main():
    main()
