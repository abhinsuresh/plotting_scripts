#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from extract_h5py import extract_band as extract_band
from tex import text as tx; from tex import print_band
from rcparams import rc_update; from tex import get_knames 
import matplotlib.gridspec as gridspec

def main():
    from matplotlib import rcParams
    ev = 0.03674932539796232
    #------------------------------------------------------------
    #------------------------------------------------------------
    # anything one wants to edit
    rc_update(rcParams, fs = 7, tex = 1, minor_y = 1, lw = 0.6)
    verbose = 1; index = '0'    
    units = 1 ; spin_p = 1              # 1: atomic; 0: ev_angstrom
    OC = 8; y_lim = 5; n_b = 4      # N occupied bands
    points = 5
    kstep  = 20
    ks = np.arange(0,kstep*points,step = kstep)
    #ks = [0, 20, 40, 60]
    if verbose : print('ksteps', ks)
    knames = get_knames('fcc_5')
    
    # Reading octopus bandstructure files
    data1            = np.loadtxt(sys.argv[1], unpack = True)
    if spin_p: data2 = np.loadtxt(sys.argv[2], unpack = True)
    ef = np.max(data1[3 + OC,:])
    #ef = -4877.42   # setting number by hand in same unit as oct
    #------------------------------------------------------------
    #------------------------------------------------------------    
    spacing   =  data1[0,:]
    kpts = [data1[0,int(i)] for i in ks]
    #------------------------------------------------------------     
    plot_x = spacing; n_p = 1
    xlabel = ""
    ylabel = "E - E_F (eV)" 
    number = '1'    # figure number
    mult_oct = 1/ev if units else  1
    mult_atk = 1/ev
    if verbose: print_band(data1, OC, mult_oct, ef, kpts) 
    #------------------------------------------------------------
    if len(sys.argv) == 3 + spin_p: n_p = 2
    gs = gridspec.GridSpec(1, 1*n_p, top=0.87,bottom=0.23,hspace = 0,
                            left=0.3-0.15*(n_p-1), right=0.7+0.2*(n_p-1),   
                            wspace=0.3)
    ax1 = plt.subplot(gs[0,0]) 
    if n_p == 2: ax2 = plt.subplot(gs[0,1])
    ax1.set_ylim((-y_lim, y_lim))
    #------------------------------------------------------------    
    # Plotting form octopus data1, and data2 if spin_p
    for i in np.arange(4,4+OC+n_b):
        ax1.plot(plot_x,     mult_oct*(data1[i,:]- ef),'k', lw = 0.2) 
    if spin_p:
        for i in np.arange(4,4+OC+n_b):
            ax1.plot(plot_x, mult_oct*(data2[i,:]- ef),'C3', 
                     lw = 0.2)#, ls=':') 
    #------------------------------------------------------------    
    # Reading and plotting data from ATK: auto detect
    if len(sys.argv) == 3 + spin_p:
        band,kpts_atk,l1 = extract_band(sys.argv[2+spin_p],verbose,index)
        spacing  = band[0,:]
        kpts_atk = [band[0,int(i)] for i in ks]
        ax2.set_ylim((-y_lim, y_lim))
        for i in np.arange(1,1+OC+n_b):
            ax2.plot(band[0,:], mult_atk*(band[i,:]),'k',lw= 0.2,
            label = 'ATK')
        if l1 == 2:
            oc2 = (band.shape[0]-1)//2
            for i in np.arange(oc2,oc2+OC+n_b):
                ax2.plot(band[0,:], mult_atk*(band[i,:]),'C3',
                label = 'ATK', lw = 0.2)
            
        ax2.set_xticks(kpts_atk)
        ax2.set_xticklabels(knames)
        ax2.vlines(x=kpts_atk, ymin=-y_lim, ymax=y_lim, 
                   lw= 0.2, color='grey', ls=':')    
        ax2.text(0.5,1.09, tx('ATK'),color='k',
                transform=ax2.transAxes,va ='top', fontsize= 6)
        ax1.text(0.2,1.09, tx('Octopus'),color='k',
                transform=ax1.transAxes,va ='top', fontsize= 6)
    #------------------------------------------------------------    
    #plt.set_xticks(kpts, knames)
    ax1.set_xticks(kpts)
    ax1.set_xticklabels(knames)
    ax1.vlines(x=kpts, ymin=-y_lim, ymax=y_lim, 
               lw= 0.2, color='grey', ls=':')    
    ax1.set_xlabel(tx(xlabel), labelpad=1)
    ax1.set_ylabel(tx(ylabel), labelpad=1)
    plt.savefig('fig' + number + '.pdf')
if __name__ == main():
    main()
    
