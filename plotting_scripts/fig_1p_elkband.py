#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np; import sys
from extract_h5py import extract_band as extract_band
from tex import text as tx; from tex import print_band
from rcparams import rc_update; from tex import get_knames 
import matplotlib.gridspec as gridspec

def main():
    from matplotlib import rcParams
    ev = 0.03674932539796232
    #------------------------------------------------------------
    #------------------------------------------------------------
    # anything one wants to edit
    rc_update(rcParams, fs = 8, tex = 1, minor_y = 1, lw = 0.6)
    verbose = 0; index = '0'    
    units = 1 ; #spin_p = 1              # 1: atomic; 0: ev_angstrom
    OC = 20; y_lim = 5; n_b = 4      # N occupied bands
    points = 5
    kstep  = 100
    ks = np.arange(0,kstep*points,step = kstep)
    #ks = [0, 20, 40, 60]
    if verbose : print('ksteps', ks)
    knames = get_knames('fcc_5')
    
    # Reading octopus bandstructure files
    data1            = np.loadtxt(sys.argv[1], unpack = True)
    data2 = np.loadtxt(sys.argv[2], unpack = True)
    #ef = np.max(data1[3 + OC,:])
    #ef = -4877.42   # setting number by hand in same unit as oct
    #------------------------------------------------------------
    #------------------------------------------------------------    
    spacing   =  data1[0,0:kstep-1]
    #kpts = [data1[0,int(i)] for i in ks]
    #------------------------------------------------------------     
    plot_x = spacing; n_p = 1
    xlabel = ""
    ylabel = "E (eV)" 
    number = '1'    # figure number
    mult_elk = 1/ev
    #------------------------------------------------------------
    n_p = 2
    gs = gridspec.GridSpec(1, 1*n_p, top=0.87,bottom=0.23,hspace = 0,
                            left=0.3-0.15*(n_p-1), right=0.7+0.2*(n_p-1),   
                            wspace=0.3)
    ax1 = plt.subplot(gs[0,0]) 
    if n_p == 2: ax2 = plt.subplot(gs[0,1])
    ax1.set_ylim((-y_lim, y_lim))
    #------------------------------------------------------------    
    # Plotting form elk data1
    for i in np.arange(OC):
        j =     i*100
        k = (i+1)*100 - 1
        if (i%2) == 0:
            ax1.plot(data1[0,j:k], data1[1,j:k], 'k', lw=0.3)
        else:
            ax1.plot(data1[0,j:k], data1[1,j:k], 'C3', lw=0.2, ls=':')
            
    ax2.set_ylim((-y_lim, y_lim))
    for i in np.arange(OC):
        j =     i*100
        k = (i+1)*100 - 1
        if (i%2) == 0:
            ax1.plot(data2[0,j:k], data2[1,j:k], 'k', lw=0.3)
        else:
            ax1.plot(data2[0,j:k], data2[1,j:k], 'C3', lw=0.2, ls=':')
        
    #ax2.set_xticks(kpts_atk)
    #ax2.set_xticklabels(knames)
    #ax2.vlines(x=kpts_atk, ymin=-y_lim, ymax=y_lim, 
               lw= 0.2, color='grey', ls=':')    
    ax2.text(0.5,1.09, tx('without SOC'),color='k',
            transform=ax2.transAxes,va ='top', fontsize= 6)
    ax1.text(0.2,1.09, tx('SOC'),color='k',
            transform=ax1.transAxes,va ='top', fontsize= 6)
    #------------------------------------------------------------    
    #plt.set_xticks(kpts, knames)
    #ax1.set_xticks(kpts)
    #ax1.set_xticklabels(knames)
    #ax1.vlines(x=kpts, ymin=-y_lim, ymax=y_lim, 
    #           lw= 0.2, color='grey', ls=':')    
    #ax1.set_xlabel(tx(xlabel), labelpad=1)
    #ax1.set_ylabel(tx(ylabel), labelpad=1)
    plt.savefig('fig' + number + '.pdf')
if __name__ == main():
    main()
    
