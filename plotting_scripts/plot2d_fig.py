import numpy as np
import h5py
import sys

sys.path.append('../../plot_manager/')
from plot2d import plotfig
scale_spin = 2

data = dict();values = dict()
f = h5py.File(sys.argv[1], 'r')
print(f.keys())
case = 'time_evo'
item = 'sz'
case = 'pscan'
#case = 'ediff'

if case =='time_evo':
    spin = f['spin'] 
    pulse = f['pulse'][:]*0.01 + 0.997
    curr = f['curr']
    print(curr.shape) 
    _time = np.arange(0,50,0.1)
    N1= 0; N2 = 500
    L = len(pulse[N1:N2])

    #one component
    #spinp = spin[5,2,N1:N2] - spin[1,2,N1:N2]
    spinp = spin[0,2,N1:N2]*scale_spin
    plot = np.concatenate((spinp.reshape(L,1),pulse[N1:N2].reshape(L,1)), axis = 1)
    if item =='sz':
        plot_x = _time; plot_y = plot
        #values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
        values['ylabel'] = r'$\mathrm{{\langle \hat{S}^z_1 \rangle}}$'
        values['xlabel'] = r'$\mathrm{Time\ (fs)}$'

    if item =='curr':
        plot_x = _time; plot_y = curr[1,:]
        #values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
        values['ylabel'] = r'$\mathrm{j}$'
        values['xlabel'] = r'$\mathrm{Time\ (fs)}$'

    if item=='fft':
        ps = np.abs(np.fft.fft(spinp))**2
        freqs = np.fft.fftfreq(spinp.size, 0.1)
        idx = np.argsort(freqs)
        plot_x = freqs*1000; plot_y = ps/100
        values['ylabel'] = r'$\mathrm{FFT\ Power\ (a.u.)}$'
        values['xlabel'] = r'$\mathrm{Frequency\ (THz)}$'

elif case=='pscan':
    item='pscan'
    spinz = f['spinz'] 
    p = f['Jz'][:]
    L = len(p)
    inp = f['input']
    print(inp[:])
    plot_x = p[:]/0.1; 
    plot_y = spinz[0,:].reshape(L,1)
    plot_y = np.concatenate((plot_y, spinz[2,:].reshape(L,1)), axis=1) 
    plot_y = np.concatenate((plot_y, spinz[5,:].reshape(L,1)), axis=1) 
    plot_y = np.concatenate((plot_y, spinz[7,:].reshape(L,1)), axis=1) 
    plot_y = plot_y*scale_spin
    print(plot_y.shape)
    #values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
    values['ylabel'] = r'$\mathrm{\langle \hat{S}^z_i \rangle\ (\hbar/2)}$'
    values['xlabel'] = r'$\mathrm{J_z/J}$'

elif case=='ediff':
    item='pscan'
    spinz = f['spinz'] 
    p = f['Jz'][:]
    L = len(p)
    inp = f['input']
    print(inp[:])
    plot_x = p[:]/0.1; 
    plot_y = f['ediff']
    print(plot_y.shape)
    #values['ylabel'] = r'$\mathrm{{\langle {\hat{S}^x}_6 \rangle} - {\langle \hat{S}^x_2 \rangle}}$'
    values['ylabel'] = r'$\mathrm{E[0] - E[1]}$'
    values['xlabel'] = r'$\mathrm{J_z/J}$'
    
data['time'] = plot_x; data['spin'] = plot_y
values['x_plot'] = 'time'; values['y_plot'] = 'spin'
plotfig(data, sys.argv[1][8:-5]+item, values, fs=8, lw=0.5, minor_y=1)

#spinp = np.concatenate((spin[3,2,:].reshape(L,1), spin[3,1,:].reshape(L,1), spin[3,0,:].reshape(L,1)),axis=1) #- spin[2,2,:]
#spinp = np.sqrt(spin[1,2,:N2]**2 + spin[1,1,:N2]**2 + spin[1,0,:N2]**2 )#- spin[2,2,:]
#print(spinp.shape)
#spinp = np.concatenate((spinp.reshape(L,1), spin[1,0,:N2].reshape(L,1), 
#                        spin[1,1,:N2].reshape(L,1)),axis=1) 
#plot = np.concatenate((spinp , pulse[:N2].reshape(L,1)), axis = 1)


#plot_x = _time[:N2]; plot_y = plot
#plot_x = _time[N1:N2]; plot_y = plot
#values['ylabel'] = r'$\mathrm{|\langle \hat{\vec{S}}_1 \rangle|}$'
#values['ylabel'] = r'$\mathrm{{|\langle \hat{\vec{S}} \rangle}_2|}$'

