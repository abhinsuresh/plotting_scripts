#!/bin/bash

grep RESULTS $1 > temp_file
grep N temp_file > $2
rm temp_file
sed -i 's/RESULTS//g' $2 
sed -i 's/([0-9]*,[0-9]*)\*N//g' $2
sed -i 's/ (\([0-9]*\),[0-9]*) /  \1  /g' $2
sed -i 's/(\(.*\),.*)/  \1  /g' $2 
